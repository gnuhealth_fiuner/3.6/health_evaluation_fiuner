# -*- coding: utf-8 -*-

from trytond.pool import Pool
from trytond.report import Report

__all__ = ['EvaluationReport']

class EvaluationReport(Report):
    'Evaluation Report'
    
    __name__ = 'gnuhealth.patient.evaluation'

    @classmethod
    def get_context(cls, records, data):
        pool = Pool()
        Evaluation = pool.get('gnuhealth.patient.evaluation')
        context = super(EvaluationReport, cls).get_context(
            records, data)

        context['evaluation'] = Evaluation.search([
                                                ('state','>=','signed')
                                                ])                     

        return context
        