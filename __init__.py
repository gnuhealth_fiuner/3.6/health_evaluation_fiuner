from trytond.pool import Pool
from .health_evaluation_fiuner import *
from .report import *
from .wizard import *

def register():
    Pool.register(
        PatientEvaluation,
        module='health_evaluation_fiuner', type_='model')
    Pool.register(
        EvaluationReport,
        module='health_evaluation_fiuner', type_='report')
    Pool.register(
        OpenAppointmentEvaluation,
        module='health_evaluation_fiuner', type_='wizard')
